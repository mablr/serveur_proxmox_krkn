# Aide au déploiement

Cette partie est fausse, pour le moment.

Vous trouverez ici toute la documentation relative au déploiement via Ansible ainsi que des notes et des conseils issue des précédents déploiement.

# Table des matières
1. [Introduction à Ansible](#)
2. [Déploiement via Ansible](deploiement_avec_ansible.md)
