# Système de sauvegarde

Vous trouverez ici toute la documentation relative à la mise en place du système de sauvegarde du serveur principal. Nous allons utiliser BorgBackup et Ansible pour les sauvergardes.

# Table des matières
1. [Présentation](presentation.md)
